Simple text adventure in C.

# TODO
- [x] implement linked list for future usage
- [x] implement undo and redo for the game
- [ ] implement min-max algorithm
- [ ] load game configurations from xml file.

