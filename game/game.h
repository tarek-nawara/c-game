/*
 * File: Game.h
 * Description: Holder for the basic representation
                of the game world.
 * Created at: 2018-1-20
 */

#ifndef _GAME_H_
#define _GAME_H_

#include <stdio.h>
#include <stdlib.h>

#include "./data/list.h"

struct player {
  char *name;
  char symbol;
  int score;
};

struct move {
  char symbol;
  int col;
  int row;
};

struct game {
  short turn;  
  int n;
  int m;
  int num_of_filled;
  struct player p_one;
  struct player p_two;
  int *available_row;
  char **board;
  struct l_node *undo;
  struct l_node *redo;
};

#include "judge.h"

/* Build the game world with the given dimensions and */
struct game *build_game(int n, int m);

/* Puts a piece in the given location on the board.
   This function will print an error if the target
   column is full and will ignore the move. */
void play(struct game *g, int col);

/* Undo the last move in the game. or
   do nothing if nothing to undo. */
void undo(struct game *g);

/* Redo the last move, or 
   do nothing if nothing to redo*/
void redo(struct game *g);

/* Print the game board on the screen. */
void pretty_print(struct game *g);

/* Free the memory allocated by the game*/
void destroy_game(struct game *g);

#endif // _GAME_H_
