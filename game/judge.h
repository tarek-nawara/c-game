/*
 * FILE: judge.h
 * Description: holding logic for calculating scores.
 * Created at: 2018-7-3
 */

#ifndef _JUDGE_H_
#define _JUDGE_H_

#include "game.h"

/* Calculate the scores of each
   Player*/
void calc_scores(struct game *g);

#endif // _JUDGE_H_
