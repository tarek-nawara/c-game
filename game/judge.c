/*
 * FILE: judge.c
 * Description: holding implementation of calculating scores
 * Created at: 2018-7-3
 */

#include "judge.h"

static void add_to_score(struct game *g, int symbol)
{
  if (symbol == g->p_one.symbol) {
    g->p_one.score++;
  } else if (symbol == g->p_two.symbol) {
    g->p_two.score++;
  }
}


static void calc_scores_rows(struct game *g)
{
  for (int i = 0; i < g->n; ++i) {
    for (int j = 0; j < g->m - 4; ++j) {
      int start = -1;
      int flag = 1;
      for (int k = 0; k < 4; ++k) {
	if (start == -1) {
	  start = g->board[i][j + k];
	} else if (start != g->board[i][j + k]) {
	  flag = 0;
	  break;
	}
      }
      if (flag) {
	add_to_score(g, start);
      }
    }
  }
}

static void calc_scores_cols(struct game *g)
{
  for (int j = 0; j < g->m; ++j) {
    for (int i = 0; i <= g->n - 4; ++i) {
      int start = -1;
      int flag = 1;
      for (int k = 0; k < 4; ++k) {
	if (start == -1) {
	  start = g->board[i + k][j];
	} else if (start != g->board[i + k][j]) {
	  flag = 0;
	  break;
	}
      }
      if (flag) {
	add_to_score(g, start);
      }
    }
  }
}

static void calc_scores_diag(struct game *g)
{
  for (int i = 0; i < g->n - 4; ++i) {
    for (int j = 0; j <= g->m - 4; ++j) {
      int start = 0;
      int flag = 1;
      for (int k = 0; k < 4; ++k) {
	if (start == -1) {
	  start = g->board[i + k][j + k];
	} else if (start != g->board[i + k][j + k]) {
	  flag = 0;
	  break;
	}
      }
      if (flag) {
	add_to_score(g, start);
      }
    }
  }
}

static void calc_scores_rev_diag(struct game *g)
{
  for (int i = 0; i <= g->n - 4; ++i) {
    for (int j = g->m - 1; j >= 4; --j) {
      int start = -1;
      int flag = 1;
      for (int k = 0; k < 4; ++k) {
	if (start == -1) {
	  start = g->board[i + k][j - k];
	} else if (start != g->board[i + k][j - k]) {
	  flag = 0;
	  break;
	}
      }
      if (flag) {
	add_to_score(g, start);
      }
    }
  }
}

void calc_scores(struct game *g)
{
  g->p_one.score = 0;
  g->p_two.score = 0;
  calc_scores_rows(g);
  calc_scores_cols(g);
  calc_scores_diag(g);
  calc_scores_rev_diag(g);
}
