/*
 * File: game.c
 * Description: Holder for the basic game functionality
 * Created at: 2018-1-20
 */

#include "game.h"

struct game *build_game(int n, int m)
{
  struct game *g = malloc(sizeof(struct game));
  g->n = n;
  g->m = m;
  g->available_row = (int *)malloc(m * sizeof(int));
  for (int i = 0; i < m; ++i) {
    g->available_row[i] = n - 1;
  }
  char **board = (char **)malloc(n * sizeof(char *));
  for (int i = 0; i < n; ++i) {
    board[i] = (char *)malloc(m * sizeof(char));
  }
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < m; ++j) {
      board[i][j] = ' ';
    }
  }
  g->board = board;
  g->turn = 0;
  g->num_of_filled = 0;
  g->p_one.score = 0;
  g->p_two.score = 0;
  g->undo = NULL;
  g->redo = NULL;
  return g;
}

void play(struct game *g, int col)
{
  if (col < 0 || col >= g->m) {
    puts("Invalid move, try again");
    return;
  }
  int row = g->available_row[col];
  if (row < 0) {
    puts("Invalid move, try again");
    return;
  }
  char symbol = 0;
  if (g->turn == 0) {
    symbol = g->p_one.symbol;
  } else {
    symbol = g->p_two.symbol;
  }
  g->board[row][col] = symbol;
  g->turn = (g->turn + 1) & 1;
  --(g->available_row[col]);
  calc_scores(g);
  struct move cur_move = { symbol, col, row };
  add(&(g->undo), &cur_move, sizeof(struct move));
  clear(&(g->redo));
}

void undo(struct game *g) {
  if (empty(&(g->undo))) {
    puts("Nothing to undo");
    return;
  }
  struct move cur_move = *(struct move *) get(g->undo, 0);
  add(&(g->redo), &cur_move, sizeof(struct move));
  g->board[cur_move.row][cur_move.col] = ' ';
  g->turn = (g->turn + 1) & 1;
  ++(g->available_row[cur_move.col]);
  calc_scores(g);
}

void redo(struct game *g) {
  if (empty(&(g->redo))) {
    puts("Nothing to redo");
    return;
  }
  struct move cur_move = *(struct move *) get(g->redo, 0);
  add(&(g->undo), &cur_move, sizeof(struct move));
  g->board[cur_move.row][cur_move.col] = cur_move.symbol;
  g->turn = (g->turn + 1) & 1;
  --(g->available_row[cur_move.col]);
  calc_scores(g);
}


void pretty_print(struct game *g)
{
  for (int j = 0; j < g->m; ++j) {
    printf("----");
  }
  puts("");
  for (int i = 0; i < g->n; ++i) {
    printf("|");
    for (int j = 0; j < g->m; ++j) {
      printf(" %c |", g->board[i][j]);
    }
    puts("");
    for (int j = 0; j < g->m; ++j) {
      printf("----");
    }
    puts("");
  }
}

void destroy_game(struct game *g)
{
  for (int i = 0; i < g->n; ++i) {
    free(g->board[i]);
  }
  free(g->board);
  free(g->available_row);
  free(g->p_one.name);
  free(g->p_two.name);
  free(g);
}
