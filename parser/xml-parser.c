/*
 * File: xml-parser.c
 * Description: holder of xml parser implementation
 * Created at: 18/8/2018
 */

#include "xml-parser.h"

static struct config default_config = {10, 10, 10};

static struct xml_tree *parse_node(FILE *fp)
{
  char cur = fgetc(fp);
  while (isspace(cur) || cur == '<') {
    cur = fgetc(fp);
  }
  struct xml_tree *tree = malloc(sizeof(struct xml_tree));
  int idx = 0;
  while (cur != '>') {
    if (isspace(cur)) {
      cur = fgetc(fp);
      continue;
    }
    tree->open_name[idx++] = cur;
    cur = fgetc(fp);
  }
  cur = fgetc(fp);
  while (isspace(cur)) {
    cur = fgetc(fp);
  }
  tree->value = 0;
  while (isdigit(cur)) {
    tree->value *= 10;
    tree->value += (cur - '0');
    cur = fgetc(fp);
  }

  // tree with children
  if (tree->value == 0) {
    while (cur == '<') {
      struct xml_tree *child = parse_node(fp);
      add(&(tree->children), &child, sizeof(struct xml_tree));
      cur = fgetc(fp);
    }
  } else {
    int idx = 0;
    while (cur != '>') {
      if (isspace(cur) || cur == '/') {
	      cur = fgetc(fp);
	      continue;
      }
      tree->close_name[idx++] = cur;
      cur = fgetc(fp);
    }
  }
  return tree;
}

struct config read_config(char *fname)
{
  FILE *fp = fopen(fname, "r");
  struct xml_tree *tree = parse_node(fp);
  return default_config;
}
