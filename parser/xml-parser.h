/*
 * File: xml-parser.h
 * Description: holding definition of xml parser functions
 * Created at: 18/8/2018
 */

#ifndef _XML_PARSER_H_
#define _XML_PARSER_H_

#include <stdio.h>
#include <ctype.h>
#include "../data/list.h"

#define MAX_LEN 100

struct config {
  int height;
  int width;
  int history_count;
};

struct xml_tree {
  char open_name[MAX_LEN];
  char close_name[MAX_LEN];
  int value;
  struct l_node *children;
};

/* Read configuration from
   the given file name. */
struct config read_config(char *fname);

#endif // _XML_PARSER_H_
