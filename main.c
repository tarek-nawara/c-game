/*
 * File: main.c
 * Description: Holding the business logic of the game
 * Created at: 2018-1-20
 */

#include <stdio.h>
#include <stdlib.h>

#include "game/game.h"
#include "parser/xml-parser.h"

#define CONFIG_PATH "config.xml"

const int N = 10;
const int M = 10;
const int NAME_LEN = 20;
const int UNDO_KEY = -1;
const int REDO_KEY = -2;

void initialize_game(struct game *g);

int main(void) {
  struct config conf = read_config(CONFIG_PATH);
  struct game *g = build_game(conf.width, conf.height);
  initialize_game(g);
  do {
    pretty_print(g);
    int col;
    printf("%s score=%d\n", g->p_one.name, g->p_one.score);
    printf("%s score=%d\n", g->p_two.name, g->p_two.score);
    printf("Undo = %d, Redo = %d\n", UNDO_KEY, REDO_KEY);
    if (g->turn == 0) {
      printf("%s move: ", g->p_one.name);
    } else {
      printf("%s move: ", g->p_two.name);
    }
    scanf("%d", &col);
    if (col == UNDO_KEY) {
      undo(g);
    } else if (col == REDO_KEY) {
      redo(g);
    } else {
      play(g, col);
    }
  } while (g->num_of_filled < M);
  destroy_game(g);
  return 0;
}

void initialize_game(struct game *g)
{
  g->p_one.name = (char *)malloc(NAME_LEN * sizeof(char));
  g->p_two.name = (char *)malloc(NAME_LEN * sizeof(char));
  puts("Enter player one's name");
  scanf("%20s", g->p_one.name);
  puts("Enter player two's name");
  scanf("%20s", g->p_two.name);
  g->p_one.symbol = 'x';
  g->p_two.symbol = 'o';
}
